FROM alpine:edge
RUN apk — update add postgresql-client erlang erlang-sasl erlang-crypto erlang-syntax-tools && rm -rf /var/cache/apk/*
ENV APP_NAME project
ENV PORT 4000
RUN mkdir -p /app
COPY $APP_NAME.tar.gz /app/
WORKDIR /app
RUN tar -zxvf $APP_NAME.tar.gz
EXPOSE $PORT
CMD trap exit TERM; /app/bin/$APP_NAME foreground & wait

